---
title: "R Notebook"
output: html_notebook
---

## Tennis Stats - analysing the data  

I really like my little Federer net-approaches hack. Decided that if I am going to write a blog post, then I should really push the boat out and get a whole lot more data.

Then I stumbled on this goldmine! https://msperlin.github.io/2017-02-05-R-and-Tennis/ Let's get into it!

Good background: http://www.tennisabstract.com/charting/meta.html  

### Download the data 

```{r}

zip.file <- 'TennisData.zip'

if (!file.exists(zip.file)){
  download.file('https://github.com/JeffSackmann/tennis_MatchChartingProject/archive/master.zip', destfile = zip.file)
  
}

unzip(zip.file)
```

Now, let's extract the net-points statistics (for men only).

```{r}
library(data.table)
library(ggplot2)

#netstats <- fread("C:/Users/NickBurns/GitLab/BayesianSandpit/ExampleCodes/TennisStats/charting-m-stats-NetPoints.csv")
netstats <- fread("Z:\\GitLab\\BayesianSandpit\\ExampleCodes\\TennisStats\\NetStats_Clean.csv")
head(netstats)
```

Groan, but no surprise, there is a massive amount of data wrangling to do here. Sigh. Tidy data anyone? Apparantly not!  

Let's strip out the match date and the player's names  


```{r}
library(rvest)

get_outcome <- function (match) {
    url <- "http://www.tennisabstract.com/charting/%s.html"
    
    winner <- ""
    try({
        page <- read_html(sprintf(url, match))
        
        outcome <- html_nodes(page, "b")[1]
        outcome <- gsub("<b>", "", outcome)
        winner <- paste0(strsplit(outcome, " ")[[1]][1:2], collapse = " ")
    })
    return (winner)
}
parse <- function (x) {

    tmp <- unlist(strsplit(x$match_id, "-")[[1]])
    
    player_a <- gsub("_", " ", ifelse(x$player == 1, tmp[5], tmp[6]))
    player_b <- gsub("_", " ", ifelse(x$player == 1, tmp[6], tmp[5]))
    winner_ <- get_outcome(x$match_id)
    winner_ <- ifelse(winner_ == "", "Unknown", ifelse(winner_ == player_a, player_a, player_b))
    
    results <- data.table(MatchDate = as.Date(tmp[1], format = "%Y%m%d"),
                          Tournament = tmp[3],
                          Player = player_a,
                          Opponent = player_b,
                          Winner = winner_,
                          NetPoints = x$net_pts,
                          PointsWonAtNet = x$pts_won)
}

filtered_stats <- netstats[row %in% c("Approach", "NetPoints")]
metadata <- rbindlist(
    lapply(1:nrow(filtered_stats),
           function (idx) {
               filtered_stats[idx, parse(.SD)]
           }))

metadata <- metadata[, .(NetPoints = sum(NetPoints), PointsWonAtNet = sum(PointsWonAtNet)),
                    by = c("MatchDate", "Tournament", "Player", "Opponent", "Winner")]

```

Now the tricky part, I had hoped that Player 1 would be the "winner" of the match, but it turns out this isn't the case. So I will have to identify who won each match as well.

Cool, that's done. Now to analyse this!  

## Overtime  

My idea is that approaches used to be high (in the 80s and early 90s), that they dropped off through th 902 and early 200s and are picking up again. Let's see if this pans out.

```{r}
by_year <- metadata[, .(AverageNetPoints = sum(NetPoints) / .N, 
                        QUpper = quantile(NetPoints, 0.75),
                        QLower = quantile(NetPoints, 0.25)), by = year(MatchDate)]
ggplot(by_year, aes(x = year, y = AverageNetPoints)) +
    geom_line(colour = "dodgerblue") +
    geom_ribbon(aes(ymin = QLower, ymax = QUpper), colour = "darkgrey", alpha = 0.25) +
    theme_minimal()
```


Hmmm, turns out this graph is poor, because we don't have many matches charted before 2012. So I will scrap the idea of a wider analysis and look at some key players


```{r}
players <- "Federer|Djokovic|Nadal|Murray|Berdych|Goffin"
ggplot(metadata[grepl(players, Player)], aes(x = NetPoints)) +
  geom_histogram(fill = "dodgerblue", colour = "dodgerblue", alpha = 0.5) +
    facet_wrap(~ Player) +
  theme_minimal()
```

Let's break this out by outcome

```{r}
players <- "Federer|Djokovic|Nadal|Murray|Berdych|Goffin"
ggplot(metadata[grepl(players, Player)], aes(x = NetPoints)) +
  geom_histogram(aes(fill = Player == Winner, colour = Player == Winner), alpha = 0.5) +
    facet_wrap(~ Player) +
  theme_minimal()
```


This is interesting. There is a pattern amongst some of these players, to push up the court more when they are losing. Very interesting. Let's look at the WinRate now...

```{r}
players <- "Federer|Djokovic|Nadal|Murray|Berdych|Goffin"
ggplot(metadata[grepl(players, Player)], aes(x = PointsWonAtNet / NetPoints)) +
  geom_histogram(aes(fill = Player == Winner, colour = Player == Winner), alpha = 0.5) +
    facet_wrap(~ Player) +
  theme_minimal()

```

This is very interesting now. As you would expect, there is an increase in the points won at net when the player is winning.


