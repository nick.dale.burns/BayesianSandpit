---
title: "R Notebook"
output: html_notebook
---

## Incremental Bayesian Models  

One of the big benefits of Bayesian modeling is the ability to lear incrementally, as data arrives. I want to investigate whether this is possible with RJAGS. My first idea is to initialise a model, burn it in and then save it. Then, we will see if we can pass in new data to the MCMC sampler.  

I'm going to use some simple test data for this. It will be a gaussian process with mean and variance that depend on one of 3 states, the idea being that we should be able to incrementally adapt our models as the process moves in and out of these states.  

Let's begin by creating our test data:  

```{r}
library(data.table)
library(ggplot2)


my.process <- function (n) {
    state <- sample(1:3, 1, prob = c(0.6, 0.3, 0.1))
    xs <- rnorm(n, mean = state, sd = state / sqrt(state))
    
    return (xs)
}

test.data <- rbindlist(
    lapply(1:10,
           function (z) {
               data.table(id = 1000*(z - 1) + c(1:1000), x = my.process(1000))
           })
)
test.data[, y := rnorm(1, 0, 100) + rpois(10000, 10) * x]
ggplot(test.data, aes(x = id, y = y)) + geom_point(colour = "darkgrey", alpha = 0.5) + geom_line(colour = "steelblue") + 
    theme_minimal()
```

Cool, there are a couple of state changes in the plot above. Let's start by simply running the full bayesian model...

```{r}
library(rjags)

model_string <- "model {
    
    for (i in 1:length(y)) {
        
        y[i] ~ dnorm(mu[i], sigma)
        mu[i] ~ ddexp(intercept * x[i], sqrt(2.0))
    }

    intercept ~ dnorm(0.0, 1.0 / 1.0e3)
    tau ~ dexp(1.0)
    sigma <- 1.0 / tau

}"

set.seed(124)
params = c("mu", "intercept", "tau")

burnin <- 500
iterations <- 100
chains <- 2

start_time <- Sys.time()

data_jags <- as.list(
    test.data
)



#### Initialise the model
# This process takes quite a while, so I am going to try and memoise the initialised model
# and see if we can just draw from this each time
print(sprintf("Beginning Model Initialisation: %s", Sys.time()))

mod = jags.model(textConnection(model_string), 
                 data = data_jags, 
                 n.chains = chains)
print(sprintf("Finished Model Initialisation: %s", Sys.time()))


#### Burnin
print(sprintf("Beginning Burn-In (%s): %s", burnin, Sys.time()))
update(mod, burnin) # burn in
print(sprintf("Finished Burn-In (%s): %s", burnin, Sys.time()))


#### MCMC
print(sprintf("Beginning Posterior Sampling (%s): %s", iterations, Sys.time()))
b.simulation <- coda.samples(model = mod, variable.names = params, n.iter = iterations)


#### Gather results
posterior_samples <- data.table(do.call(rbind, b.simulation))


plot(b.simulation)       
```