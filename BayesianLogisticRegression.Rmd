---
title: "R Notebook"
output: html_notebook
---

# Bayesian logistic models  

Here, we will consider both standard logistic models. For the standard logistic regression we will use the urine dataset from the boot package. For the hierarchical logsitic regression, we will use the National Health 2008 dataset from a Harvard tutorial (http://tutorials.iq.harvard.edu/R/Rstatistics/Rstatistics.html#exercise_2:_logistic_regression)  

## Example 1: Urine dataset

This dataset comes from the boot package. The 'r' variable is the 'response' variable and indicates the presence of calcium oxalate cyrstals in the urine. There are 6 covariates which relate to various chemical properties of the samples. There are no 'groups' or obvious correlations in this dataset, so we can assume exchangeability (and independence) across all observations and fit a standard linear regression.

```{r}
library(data.table)
library(rjags)
library(boot)

data(urine)
head(urine)
```

We begin by standardising the features and then with a quick visualisation of the features:

```{r}
urine.x <- data.table(scale(urine[, -1]))
urine.x[, r := urine$r]

for (x in colnames(urine.x)) {
  if (x != 'r') boxplot(urine.x[[x]] ~ urine.x$r, main = x)
}
```

Clearly, there are some strong associations here with the presence of calcium crystals. Most notably, the calcium concentration.

Let's fit a standard GLM as reference.

```{r}
ref_model <- glm(factor(r) ~ ., data = urine.x, family = "binomial")
summary(ref_model)

print(sprintf("Chi-squared test of model adequacy: %s", round(1 - pchisq(ref_model$deviance, ref_model$df.residual), 3)))
```

This first model is not an adequate fit (test < 0.95). This is easily explained by the number of unrelated features. not suprisingly, the calcium levels in the urine have the strongest association with the presence of calcium oxalte crystals. The urea concentration also plays a role by the look of this.

Let's refit with just these two features.

```{r}
ref_model <- glm(factor(r) ~ calc + urea, data = urine.x, family = "binomial")
summary(ref_model)

print(sprintf("Chi-squared test of model adequacy: %s", round(1 - pchisq(ref_model$deviance, ref_model$df.residual), 3)))
```

This is certainly interesting, the AIC is worse as is the adequacy of the fit. Let's try once more, but include two-way interactions:


```{r}
ref_model <- step(glm(factor(r) ~ .^2, data = urine.x, family = "binomial"), trace = FALSE)
summary(ref_model)

print(sprintf("Chi-squared test of model adequacy: %s", round(1 - pchisq(ref_model$deviance, ref_model$df.residual), 3)))
```

The test for adequacy is mow borderline at 0.956. Clearly, the step function hasn't removed all of the features that do not show a significant effect. Let's do this and rerun the model.

```{r}
ref_model <- glm(factor(r) ~ gravity + calc + urea + cond + osmo + urea:calc + cond:calc + osmo:calc, 
                 data = urine.x, family = "binomial")
summary(ref_model)

print(sprintf("Chi-squared test of model adequacy: %s", round(1 - pchisq(ref_model$deviance, ref_model$df.residual), 3)))
```

Despite the lack of significance in some of these terms, the model is approaching an adequate fit (maybe...) and this is the lowest AIC yet. Let's perform a 10 fold cross validation and assess the predictive performance:  

```{r}
urine.x[, Fold := sample(10, nrow(urine.x), replace = TRUE)]

for (k in 1:10) {
  idx <- urine.x[, Fold != k]
  model <- glm(factor(r) ~ gravity + calc + urea + cond + osmo + urea:calc + cond:calc + osmo:calc, 
                 data = urine.x[idx], family = "binomial")
  predictions <- predict(model, urine.x[-idx], type = "response") # this should give us predicted probabilities
  urine.x[-idx, Prediction := predictions]
}

plot(urine.x$Prediction, jitter(urine.x$r))
```

Despite the borderline adequacy of the fit, the out-of-bag predictions are quite good. I suspect that the lack of data is what is throwing the model accuracy. Let's be happy with this and look to the Bayesian method.

### Bayesian Logistic Regression  

First, let's specify our model. We will begin with a baseline model without any interaction terms.  

```{r}
model_string <- "model {
	for (i in 1:length(y)) {
		y[i] ~ dbern(p[i])
		logit(p[i]) <- intercept + b[1]*gravity[i] + b[2]*ph[i] + b[3]*osmo[i] + b[4]*cond[i] + b[5]*urea[i] + b[6]*calc[i]
	}
	
  intercept ~ dnorm(0.0, 1.0 / 100.0)
	for (j in 1:6) {
		b[j] ~ ddexp(0.0, sqrt(2.0)) # has variance 1.0
	}

}"
```

For the priors we have selected:  

  - a relatively wide, uniformative prior on the intercept term  
  - very selective priors, via the couble exponential on the betas. The goal here, is to perform feature selection as we fit using this highly constrained prior.  
  
Let's fit this model and look at the convergence diagnostics:


HA! Turns out there are cases with missing data, we will drop these two.  

```{r}
model_data <- as.list(urine.x[complete.cases(urine.x), .(y = r, gravity, ph, osmo, cond, urea, calc)])
model <- jags.model(textConnection(model_string),
                   data = model_data,
                   n.chains = 3)
params <- c("intercept", "b")

update(model, 5e3)
mcmc.samples <- coda.samples(model, params, 2e4)


# convergence properties:
plot(mcmc.samples)

gelman.diag(mcmc.samples)
autocorr.diag(mcmc.samples)
autocorr.plot(mcmc.samples)
effectiveSize(mcmc.samples)

## calculate DIC
dic1 = dic.samples(model, n.iter=1e4)
dic1
```

There is still quite a bit of autocorrelation in these posterior distributions. That said, the initial distributions support the feature selection found with the GLM. I am going to boost this to have 5000 iterations of burn in and then a further 20,000 iterations to converge.

Posterior estimates over the features:  

  - Gravity and calc have a positive effect on the odds of calcium crystals  
  - ph, osmo and the intercept are non-informative  
  - urea and cond have a negative effect on the odds of calcium crystals  
  
The effective sample sizes are quite low, due to the autocorrelation in these samples. I should probably prune the posterior samples to deal with this, but will leave for now. That said, the Gelman statistics are all 1 which strongly suggests convergence (along with the trace plots).  


What we will do instead, is re-model without ph and osmolarity. I also tried a couple of interaction terms, but these were non-informative. 

```{r}
model_string <- "model {
	for (i in 1:length(y)) {
		y[i] ~ dbern(p[i])
		logit(p[i]) <- intercept + b[1]*gravity[i] + b[2]*cond[i] + b[3]*urea[i] + b[4]*calc[i]
	}
	
  intercept ~ dnorm(0.0, 1.0 / 100.0)
	for (j in 1:4) {
		b[j] ~ ddexp(0.0, sqrt(2.0)) # has variance 1.0
	}

}"


model_data <- as.list(urine.x[complete.cases(urine.x), .(y = r, gravity, cond, urea, calc)])
model <- jags.model(textConnection(model_string),
                   data = model_data,
                   n.chains = 3)
params <- c("intercept", "b")

update(model, 5e3)
mcmc.samples <- coda.samples(model, params, 2e4)


# convergence properties:
plot(mcmc.samples)

gelman.diag(mcmc.samples)
autocorr.diag(mcmc.samples)
autocorr.plot(mcmc.samples)
effectiveSize(mcmc.samples)

## calculate DIC
dic2 = dic.samples(model, n.iter=1e4)
dic2
```


Good, we've got a slightly lower penalised DIC, the posterior distributions support out earlier hypothesis and the effective sample size on cond and calc are very high. All-in-all this is a great model.  

### Posterior inference  

The first thing to do, is to predict the posterior probabilities for each of our samples. We should have done this out-of-bag, but let's see what happens all the same

```{r}
estimates <- colMeans(as.mcmc(do.call(rbind, mcmc.samples)))

# we're going ignore the intercept, as this was non-informative
posteriorp <- function (x, estimates) {
  z <- as.matrix(x) %*% estimates
  
  return (1 / (1 + exp(-z)))
}
urine.x[, PosteriorP := unlist(lapply(1:nrow(urine.x), 
                                      function (id) posteriorp(urine.x[id, .(gravity, cond, urea, calc)], estimates[1:4])))]
plot(urine.x$PosteriorP, jitter(urine.x$r))
```

This plot is very positive, and very comparable to the GLM. Let's look at the predictive accuracies of the two methods:  

```{r}
table(urine.x[, .(r, GLMPrediction = Prediction >= 0.5)])
table(urine.x[, .(r, GLMPrediction = PosteriorP >= 0.5)])
```

Though these weren't quite apples for apples, they are comparable and that's what matters.  

We now need to look at posterior inferences...

Firstly, let's look at the probability of calcium crystals for a single observation. Here, we will use the posterior MCMC samples to get a rnage of estimated betas and apply this to get a range of estimated probabilities for our example. We will do this for a range of observations and compare their predicted probabilities: 

```{r}
observations <- c(2, 3, 62, 67)
post_estimates <- as.mcmc(do.call(rbind, mcmc.samples))

posterior_distributions <- list()
for (obs in observations) {
  tmp <- urine.x[obs, .(gravity, cond, urea, calc)]
  probs <- posteriorp(tmp, t(post_estimates[, 1:4]))
  posterior_distributions[[obs]] <- probs
  
  
  dx <- density(probs)
  if (obs == 2) {
    plot(dx, xlim = c(0, 1), ylim = c(0, 10))
  } else {
    lines(dx)
  }
  text(x = dx$x[which.max(dx$y)], dx$y[which.max(dx$y)] + 1, labels = sprintf("Obs %s", obs))
}

```

This is great, we can see the posterior predictive distributions of each of thes observations. From this, we could conclude:  

  - Observation 3 is highly likely to not develop calcium crystals  
  - Conversely, observation 67 is highly likely to develop calcium crystals  

Observations 2 and 62 are more interesting, let's consider the following:

```{r}
round(100 * sum(posterior_distributions[[2]] > 0.5) / 60000, 2)
round(100 * sum(posterior_distributions[[62]] > 0.5) / 60000, 2)
```

Based on the above, I would conclude that Observation 2 has a ~29% chance of developing calcium cyrstals whereas, Observation 62 has as high as ~89% chance! This is very interesting, note that these two estimates are quite different to the mean estimated probabilities, because they account for all of the uncertainty in the estimate as well as the skew in the posterior distributions (which relate to the uncertainty). 

There's another story to be told here as well. Using the posterior distributions, we can directly compare each of the observations:

```{r}
comparisons <- matrix(rep(0, 16), nrow=4)
colnames(comparisons) <- paste("Obs ", observations, sep = "")
rownames(comparisons) <- paste("Obs ", observations, sep = "")
for (i in 1:4) {
  print(observations[i])
  for (j in 1:4) {
    z <- round(sum(posterior_distributions[[observations[i]]] > posterior_distributions[[observations[j]]]) / 60000, 4)
    comparisons[i, j] <- z
  }
}
comparisons
```

This works specatularly well. You need to take a little care to ensure that you are looking at the correct distributions, but these infernces hold incredibly well. So, not only can we accurately estimate the probabilities of developing calcium crystals, we can also compare observations against each other. For example, we can say that Observation 67 has a 78% higher chance of developing crystals than Observation 62. And here's the good part, there is the exact counter to this, that there is a 22% chance that Observation 62 is at greater risk than Observation 67. Under the Bayesian approach, it is perfectly sensible to make these inferences, which we couldn't do reliably under the frequentist approach.  


And this, is how we would accurately model the probabilities of winning a quote for DD. Posterior mean estimates are interesting, but if we draw large enough posterior samples (which we certainly have here), then we can provide a stronger case for the probability of winning a quote. We would still have to manipulate the margin, but that is somewhat irrelevant. The posterior inferences would allow us to accurately estimate the probability of winning and would allow us to directly compare quotes at different margins.  